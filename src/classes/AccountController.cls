/**
 * Created by alexanderbelenov on 28.05.2022.
 */

@RestResource(UrlMapping='/accounts')
global with sharing class AccountController {
    private final static AccountService accountService = new AccountService();

    @HttpPost
    global static List<Id> insertAccounts(List<Account> accounts) {
//        List<Account> accountsToUpdate = new List<Account>();
//        for (Account account : accounts) {
//            accountsToUpdate.add(account.clone(false));
//        }
        return accountService.insertAccounts(accounts);
    }
}