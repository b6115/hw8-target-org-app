/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class AccountService {

    public List<Id> insertAccounts(List<Account> accounts) {
        List<Account> updatedAccountList = this.getUpdatedAccounts(accounts);
        insert updatedAccountList;
        return this.getIDs(updatedAccountList);
    }

    private List<Account> getUpdatedAccounts(List<Account> accounts) {
        final List<Account> updatedAccounts = new List<Account>();
        for (Account account : accounts) {
            final Account clonedAccount = account.clone(true, true);
            clonedAccount.OriginalSource__c = account.AccountSource;
            clonedAccount.AccountSource = DEFAULT_ACCOUNT_SOURCE;
            updatedAccounts.add(clonedAccount);
        }

        return updatedAccounts;
    }

    public List<Id> getIDs(List<Account> accounts) {
        final List<Id> idList = new List<Id>();
        for (Account account : accounts) {
            idList.add(account.Id);
        }
        return idList;
    }

    private final static String DEFAULT_ACCOUNT_SOURCE = 'external account';
}